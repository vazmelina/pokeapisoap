package com.pokemon.ws.api.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pokemon"
})
@XmlRootElement(name = "BaseExperienceDetailsResponse")
public class BaseExperienceDetailsResponse {

    @XmlElement(name = "Pokemon", required = true)
    protected Pokemon pokemon;

    /**
     * Obtiene el valor de la propiedad pokemon.
     * 
     * @return
     *     possible object is
     *     {@link Pokemon }
     *     
     */
    public Pokemon getPokemon() {
        return pokemon;
    }

    /**
     * Define el valor de la propiedad pokemon.
     * 
     * @param value
     *     allowed object is
     *     {@link Pokemon }
     *     
     */
    public void setPokemon(Pokemon value) {
        this.pokemon = value;
    }

}
