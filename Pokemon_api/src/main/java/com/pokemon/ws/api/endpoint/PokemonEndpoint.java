package com.pokemon.ws.api.endpoint;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.pokemon.ws.api.model.AbilitiesDetailsRequest;
import com.pokemon.ws.api.model.AbilitiesDetailsResponse;
import com.pokemon.ws.api.model.BaseExperienceDetailsRequest;
import com.pokemon.ws.api.model.BaseExperienceDetailsResponse;
import com.pokemon.ws.api.model.HeldItemsDetailsRequest;
import com.pokemon.ws.api.model.HeldItemsDetailsResponse;
import com.pokemon.ws.api.model.IdDetailsRequest;
import com.pokemon.ws.api.model.IdDetailsResponse;
import com.pokemon.ws.api.model.LocationAreaEncountersDetailsRequest;
import com.pokemon.ws.api.model.LocationAreaEncountersDetailsResponse;
import com.pokemon.ws.api.model.NameDetailsRequest;
import com.pokemon.ws.api.model.NameDetailsResponse;
import com.pokemon.ws.api.model.Pokemon;
import com.pokemon.ws.api.model.enums.PokemonMetodosEnum;
import com.pokemon.ws.api.service.IPokemonService;

@Endpoint
public class PokemonEndpoint {

	private HttpServletRequest httpServletRequest;

	@Autowired
	IPokemonService pokemonService;

	@Autowired
	public void setRequest(HttpServletRequest request) {
		this.httpServletRequest = request;
	}

	private static final String NAMESPACE_URI = "http://www.bankaya.com/xml/pokemon";

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "NameDetailsRequest")
	@ResponsePayload
	public NameDetailsResponse getNamePokemon(@RequestPayload NameDetailsRequest request) {
		NameDetailsResponse response = new NameDetailsResponse();
		response.setPokemon(pokemonService.getPokemonInfo(this.httpServletRequest.getRemoteAddr(), request.getPokemon(),
				PokemonMetodosEnum.NAME));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "IdDetailsRequest")
	@ResponsePayload
	public IdDetailsResponse getIdPokemon(@RequestPayload IdDetailsRequest request) {
		IdDetailsResponse response = new IdDetailsResponse();
		response.setPokemon(pokemonService.getPokemonInfo(this.httpServletRequest.getRemoteAddr(), request.getPokemon(),
				PokemonMetodosEnum.ID));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "AbilitiesDetailsRequest")
	@ResponsePayload
	public AbilitiesDetailsResponse getAbilitiesPokemon(@RequestPayload AbilitiesDetailsRequest request) {
		AbilitiesDetailsResponse response = new AbilitiesDetailsResponse();
		response.setPokemon(pokemonService.getPokemonInfo(this.httpServletRequest.getRemoteAddr(), request.getPokemon(),
				PokemonMetodosEnum.ABILITIES));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "BaseExperienceDetailsRequest")
	@ResponsePayload
	public BaseExperienceDetailsResponse getExperiencePokemon(@RequestPayload BaseExperienceDetailsRequest request) {
		BaseExperienceDetailsResponse response = new BaseExperienceDetailsResponse();
		response.setPokemon(pokemonService.getPokemonInfo(this.httpServletRequest.getRemoteAddr(), request.getPokemon(),
				PokemonMetodosEnum.BASE_EXPERIENCE));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "HeldItemsDetailsRequest")
	@ResponsePayload
	public HeldItemsDetailsResponse getItemPokemon(@RequestPayload HeldItemsDetailsRequest request) {
		HeldItemsDetailsResponse response = new HeldItemsDetailsResponse();
		response.setPokemon(pokemonService.getPokemonInfo(this.httpServletRequest.getRemoteAddr(), request.getPokemon(),
				PokemonMetodosEnum.HELD_ITEMS));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "LocationAreaEncountersDetailsRequest")
	@ResponsePayload
	public LocationAreaEncountersDetailsResponse getLocationPokemon(
			@RequestPayload LocationAreaEncountersDetailsRequest request) {
		LocationAreaEncountersDetailsResponse response = new LocationAreaEncountersDetailsResponse();
		response.setPokemon(pokemonService.getPokemonInfo(this.httpServletRequest.getRemoteAddr(), request.getPokemon(),
				PokemonMetodosEnum.LOCATION_AREA_COUNTERS));
		return response;
	}
}