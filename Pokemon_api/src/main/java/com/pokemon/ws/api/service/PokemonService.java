package com.pokemon.ws.api.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.pokemon.ws.api.entity.PokemonH2;
import com.pokemon.ws.api.model.Pokemon;
import com.pokemon.ws.api.model.enums.PokemonMetodosEnum;
import com.pokemon.ws.api.repository.PokemonRepository;

@Service
public class PokemonService implements IPokemonService{

	@Autowired
	PokemonRepository pokemonRepository;
	
	@Override
	public Pokemon getPokemonInfo(String ip, String nombre, PokemonMetodosEnum metodo) {
		Pokemon pokemon = getPokemon(nombre);

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
				
		PokemonH2 pokemonH2= new PokemonH2();
		pokemonH2.setFecha(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		pokemonH2.setIp(ip);
		
		switch(metodo) {
		case ABILITIES:
			Pokemon abilities = new Pokemon();
			abilities.setAbilities(pokemon.getAbilities());
			pokemonH2.setMetodo(metodo.toString());
			pokemonRepository.save(pokemonH2);
			return abilities;
		case NAME:
			Pokemon name = new Pokemon();
			name.setName(pokemon.getName());
			pokemonH2.setMetodo(metodo.toString());
			pokemonRepository.save(pokemonH2);
			return name;
		case BASE_EXPERIENCE:
			Pokemon baseExperience = new Pokemon();
			baseExperience.setBaseExperience(pokemon.getBaseExperience());
			pokemonH2.setMetodo(metodo.toString());
			pokemonRepository.save(pokemonH2);
			return baseExperience;
		case HELD_ITEMS:
			Pokemon heldItems = new Pokemon();
			heldItems.setHeldItems(heldItems.getHeldItems());
			pokemonH2.setMetodo(metodo.toString());
			pokemonRepository.save(pokemonH2);
			return heldItems;
		case ID:
			Pokemon id = new Pokemon();
			id.setId(pokemon.getId());
			pokemonH2.setMetodo(metodo.toString());
			pokemonRepository.save(pokemonH2);
			return id;
		case LOCATION_AREA_COUNTERS:
			Pokemon locationAreaCounters = new Pokemon();
			locationAreaCounters.setLocationAreaEncounters(pokemon.getLocationAreaEncounters());
			pokemonH2.setMetodo(metodo.toString());
			pokemonRepository.save(pokemonH2);
			return locationAreaCounters;
		}
		

		return null;
	}
	
	private Pokemon getPokemon(String nombre) {
		Pokemon pokemon = new Pokemon();
		HttpClient httpclient = new DefaultHttpClient();

	    // Prepare a request object
	    HttpGet httpget = new HttpGet("https://pokeapi.co/api/v2/pokemon/"+nombre); 

	    // Execute the request
	    HttpResponse response;
	    try {
	        response = httpclient.execute(httpget);
	        // Examine the response status

	        // Get hold of the response entity
	         org.apache.http.HttpEntity entity = response.getEntity();
	         if (entity != null) {
	              InputStream instream = entity.getContent();
	              String result= convertStreamToString(instream);
	              Gson gson = new Gson();
	              pokemon = gson.fromJson(result, Pokemon.class);
	              instream.close();
	         }

	    } catch (Exception e) {}
		return pokemon;
	}
	
	 private static String convertStreamToString(InputStream is) {
		    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		    StringBuilder sb = new StringBuilder();

		    String line = null;
		    try {
		        while ((line = reader.readLine()) != null) {
		            sb.append(line + "\n");
		        }
		    } catch (IOException e) {
		        e.printStackTrace();
		    } finally {
		        try {
		            is.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		    return sb.toString();
		}

}
