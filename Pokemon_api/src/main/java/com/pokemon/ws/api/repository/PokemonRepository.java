package com.pokemon.ws.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.pokemon.ws.api.entity.PokemonH2;

public interface PokemonRepository extends CrudRepository<PokemonH2, Long>{}

