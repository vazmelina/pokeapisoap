package com.pokemon.ws.api.model;

import javax.xml.bind.annotation.XmlRegistry;


@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.pokemon.ws.api.model
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LocationAreaEncountersDetailsResponse }
     * 
     */
    public LocationAreaEncountersDetailsResponse createLocationAreaEncountersDetailsResponse() {
        return new LocationAreaEncountersDetailsResponse();
    }

    /**
     * Create an instance of {@link Pokemon }
     * 
     */
    public Pokemon createPokemon() {
        return new Pokemon();
    }

    /**
     * Create an instance of {@link AbilitiesDetailsResponse }
     * 
     */
    public AbilitiesDetailsResponse createAbilitiesDetailsResponse() {
        return new AbilitiesDetailsResponse();
    }

    /**
     * Create an instance of {@link IdDetailsResponse }
     * 
     */
    public IdDetailsResponse createIdDetailsResponse() {
        return new IdDetailsResponse();
    }

    /**
     * Create an instance of {@link LocationAreaEncountersDetailsRequest }
     * 
     */
    public LocationAreaEncountersDetailsRequest createLocationAreaEncountersDetailsRequest() {
        return new LocationAreaEncountersDetailsRequest();
    }

    /**
     * Create an instance of {@link NameDetailsRequest }
     * 
     */
    public NameDetailsRequest createNameDetailsRequest() {
        return new NameDetailsRequest();
    }

    /**
     * Create an instance of {@link AbilitiesDetailsRequest }
     * 
     */
    public AbilitiesDetailsRequest createAbilitiesDetailsRequest() {
        return new AbilitiesDetailsRequest();
    }

    /**
     * Create an instance of {@link IdDetailsRequest }
     * 
     */
    public IdDetailsRequest createIdDetailsRequest() {
        return new IdDetailsRequest();
    }

    /**
     * Create an instance of {@link BaseExperienceDetailsResponse }
     * 
     */
    public BaseExperienceDetailsResponse createBaseExperienceDetailsResponse() {
        return new BaseExperienceDetailsResponse();
    }

    /**
     * Create an instance of {@link HeldItemsDetailsResponse }
     * 
     */
    public HeldItemsDetailsResponse createHeldItemsDetailsResponse() {
        return new HeldItemsDetailsResponse();
    }

    /**
     * Create an instance of {@link NameDetailsResponse }
     * 
     */
    public NameDetailsResponse createNameDetailsResponse() {
        return new NameDetailsResponse();
    }

    /**
     * Create an instance of {@link HeldItemsDetailsRequest }
     * 
     */
    public HeldItemsDetailsRequest createHeldItemsDetailsRequest() {
        return new HeldItemsDetailsRequest();
    }

    /**
     * Create an instance of {@link BaseExperienceDetailsRequest }
     * 
     */
    public BaseExperienceDetailsRequest createBaseExperienceDetailsRequest() {
        return new BaseExperienceDetailsRequest();
    }

}
