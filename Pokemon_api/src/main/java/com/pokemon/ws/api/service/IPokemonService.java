package com.pokemon.ws.api.service;

import com.pokemon.ws.api.model.Pokemon;
import com.pokemon.ws.api.model.enums.PokemonMetodosEnum;

public interface IPokemonService {
	public Pokemon getPokemonInfo(String ip, String nombre,PokemonMetodosEnum metodo);
}
