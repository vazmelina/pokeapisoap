package com.pokemon.ws.api.model.enums;

public enum PokemonMetodosEnum {
ABILITIES,
BASE_EXPERIENCE,
HELD_ITEMS,
ID,
NAME,
LOCATION_AREA_COUNTERS
}
