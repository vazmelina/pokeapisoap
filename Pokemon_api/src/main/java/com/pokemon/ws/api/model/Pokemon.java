package com.pokemon.ws.api.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pokemon.ws.api.beans.Ability;
import com.pokemon.ws.api.beans.HeldItem;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Pokemon", propOrder = {
    "abilities",
    "baseExperience",
    "heldItems",
    "id",
    "name",
    "locationAreaEncounters"
})
public class Pokemon {

    @XmlElement(required = true)
    @SerializedName("abilities")
    @Expose
    private List<Ability> abilities = null;
    public List<Ability> getAbilities() {
		return abilities;
	}
	public void setAbilities(List<Ability> abilities) {
		this.abilities = abilities;
	}
	public Integer getBaseExperience() {
		return baseExperience;
	}
	public void setBaseExperience(Integer baseExperience) {
		this.baseExperience = baseExperience;
	}
	public List<HeldItem> getHeldItems() {
		return heldItems;
	}
	public void setHeldItems(List<HeldItem> heldItems) {
		this.heldItems = heldItems;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocationAreaEncounters() {
		return locationAreaEncounters;
	}
	public void setLocationAreaEncounters(String locationAreaEncounters) {
		this.locationAreaEncounters = locationAreaEncounters;
	}
	@XmlElement(required = true)
    @SerializedName("base_experience")
    @Expose
    private Integer baseExperience;
    @XmlElement(required = true)
    @SerializedName("held_items")
    @Expose
    private List<HeldItem> heldItems = null;
    @XmlElement(required = true)
    @SerializedName("id")
    @Expose
    private Integer id;
    @XmlElement(required = true)
    @SerializedName("name")
    @Expose
    private String name;
    @XmlElement(required = true)
    @SerializedName("location_area_encounters")
    @Expose
    private String locationAreaEncounters;

}
