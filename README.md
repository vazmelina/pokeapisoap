#Pokemon SOAP
Proyecto Soap que permite obtener Datos especificos de un pokemon

Comenzando 🚀
Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

Mirar instrucciones para conocer como desplegar el proyecto.

Pre-requisitos 📋
Java 1.8
Tener SOAPUI o cualquier cliente de servicios SOAP
Tener IDE de Java instalado

Clonar proyecto del repositorio🔧
https://bitbucket.org/vazmelina/pokeapisoap/src/develop/


-Instalacion del proyecto en equipo local
Importar el proyecto como Maven project
Correr maven clean
Correr maven install

Ubicar en ./Pokemon_api/target el jar Pokemon-0.0.1-SNAPSHOT.jar
Ejecutar comando java -jar Pokemon-0.0.1-SNAPSHOT.jar en consola de sistema para levantar el proyecto o se puede levantar desde el IDE utilizado corriendo el proyecto como Springboot App

Ejecutando las pruebas ⚙️
Una vez corriendo el proyecto
Para probar el proyecto abrir programa SOAPUI
Ingresar el siguiente endpoint WSDL para obtener las operaciones :
http://localhost:8080/service/PokemonDetailsWsdl.wsdl

Todas las operaciones aceptan como parametro de entrada el nombre del pokemon

Para comprobar la informacion guardada en DB se utilizo la base de datos H2
El acceso a la consola es http://localhost:8080/h2-console/login.jsp
usuario:sa
password:password

Ejecutar el siguiente query para validar la informacion
SELECT * FROM POKEMON;
